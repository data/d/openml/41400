# OpenML dataset: osales

https://www.openml.org/d/41400

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Multivariate regression data set from: https://link.springer.com/article/10.1007%2Fs10994-016-5546-z : This is a pre-processed version of the dataset used in Kaggles Online Product Sales competition (Kaggle 2012) that concerns the prediction of the online sales of consumer products. Each row in the dataset corresponds to a different product that is described by various product features as well as features of an advertising campaign. There are 12 target variables corresponding to the monthly sales for the first 12 months after the product launches. For the purposes of this study we removed examples with missing values in any target variable (112 out of 751) and attributes with one distinct value (145 out of 558).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41400) of an [OpenML dataset](https://www.openml.org/d/41400). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41400/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41400/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41400/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

